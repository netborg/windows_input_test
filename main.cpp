/*
Copyright (c) 2023 netborg

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. */

#include <windows.h>
#include <stdlib.h>
#include <string.h>
#include <tchar.h>
#include <strsafe.h>

#include <string>
#include <chrono>
#include <thread>
#include <ctime>
#include "GameLoop.h"
#include "RollingStandardDeviation.h"

using namespace std;

static TCHAR szWindowClass[] = _T("DesktopApp");
static TCHAR szTitle[] = _T("Windows Input Test");

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

string to_fixed_string(float v) {
	int i = v*100;
	int sub = abs(i%100);
	return to_string(i/100) + "."
		+ (sub<10 ? "0" : "") + to_string(sub);
}


int CALLBACK WinMain(
	_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPSTR     lpCmdLine,
	_In_ int       nCmdShow
) {

	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, IDI_APPLICATION);
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, IDI_APPLICATION);

	if (!RegisterClassEx(&wcex)) {
		MessageBox(NULL,
			_T("Call to RegisterClassEx failed!"),
			_T("Windows Input Test"),
			0);
		return 1;
	}


	// https://learn.microsoft.com/en-us/windows/win32/inputdev/using-raw-input#example-2
	// set RIDEV_NOLEGACY to prevent non-raw input messages from being generated

	RAWINPUTDEVICE Rid[2];

	Rid[0].usUsagePage = 0x01;
	Rid[0].usUsage = 0x02; // mouse
	Rid[0].dwFlags = 0;//RIDEV_NOLEGACY;
	Rid[0].hwndTarget = 0;

	Rid[1].usUsagePage = 0x01;
	Rid[1].usUsage = 0x06; // keyboard
	Rid[1].dwFlags = RIDEV_NOLEGACY;
	Rid[1].hwndTarget = 0;


	if (RegisterRawInputDevices(Rid, 2, sizeof(Rid[0])) == FALSE) {
		OutputDebugString(TEXT("Registering Raw Input Failed"));
		exit(-1);
	}
	

	HWND hWnd = CreateWindow(
		szWindowClass,
		szTitle,
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT,
		500, 500,
		NULL,
		NULL,
		hInstance,
		NULL
	);

	if (!hWnd) 	{
		MessageBox(NULL,
			_T("Call to CreateWindow failed!"),
			_T("Windows Input Test"),
			0);
		return 1;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);


	if (mouse_events_query_type == GET_MESSAGE) {

		start_game_loop();

		MSG msg;
		while (GetMessage(&msg, NULL, 0, 0)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

	}
	else if (mouse_events_query_type == PEEK_MESSAGE) {

		game_loop_is_running = true;

		while (game_loop_is_running) {
			game_loop_main();  // calls PeekMessage()
		}

	}

	return 0;
}




LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {

	static auto time = std::chrono::high_resolution_clock::now();
	static LPBYTE lpb = nullptr;
	static UINT lpb_maxsize = 0;

	PAINTSTRUCT ps;
	HDC hdc;
	string greeting("Hello, Windows input test!");
	string str;
	string _true ("  TRUE ");
	string _false(" FALSE ");
	string _empty("                                                              ");

	switch (message)
	{

		case WM_CREATE:

			SetTimer(hWnd, 1, 10, NULL);
			break;

		case WM_TIMER:

			InvalidateRect(hWnd, NULL, FALSE);
			break;

		case WM_PAINT:

			hdc = BeginPaint(hWnd, &ps);
			TextOut(hdc, 5, 5, greeting.c_str(), greeting.size());

			str  = "Average mouse event time elapsed: ";
			str += to_fixed_string(mouse_events_stats_avg) + " us " + _empty;

			TextOut(hdc, 5, 30, str.c_str(), str.size());

			str  = "Average mouse event std deviation: ";
			str += to_fixed_string(mouse_events_stats_std_dev) + " us " + _empty;

			TextOut(hdc, 5, 45, str.c_str(), str.size());

			str = string("Key is Down: ") +
				(key_press_state_async_query ? _true : _false) +
				(key_press_state_windows_events ? _true : _false);

			TextOut(hdc, 5, 65, str.c_str(), str.size());

			str  = "Message Delay: ";
			str += to_fixed_string(key_press_delay) + " us, ";
			str += to_fixed_string(key_release_delay) + " us" + _empty;

			TextOut(hdc, 5, 80, str.c_str(), str.size());

			EndPaint(hWnd, &ps);
			break;

		case WM_DESTROY:

			if (mouse_events_query_type == GET_MESSAGE) {
				end_game_loop();
			}

			if (mouse_events_query_type == PEEK_MESSAGE) {
				game_loop_is_running = false;
			}

			PostQuitMessage(0);
			delete[] lpb;
			exit(0);
			break;

		case WM_KEYDOWN: // measure non-raw input

			if ((!key_press_state_windows_events) && wParam == debug_key) {

				key_press_state_windows_events = true;
				key_pressed_time_windows_events = std::chrono::high_resolution_clock::now();
				auto passed_time = key_pressed_time_windows_events - key_pressed_time_async_query;

				key_press_delay = std::chrono::duration<float, std::micro>(passed_time).count();

			}

			break;

		case WM_KEYUP: // measure non-raw input

			if ((key_press_state_windows_events) && wParam == debug_key) {

				key_press_state_windows_events = false;
				key_released_time_windows_events = std::chrono::high_resolution_clock::now();
				auto passed_time = key_released_time_windows_events - key_released_time_async_query;

				key_release_delay = std::chrono::duration<float, std::micro>(passed_time).count();

			}

			break;

		case WM_INPUT: {

			UINT dwSize;
			TCHAR szTempOutput[256];

			GetRawInputData((HRAWINPUT)lParam, RID_INPUT, NULL, &dwSize,
				sizeof(RAWINPUTHEADER));

			if (dwSize > lpb_maxsize) {

				delete[] lpb;
				lpb = new BYTE[dwSize];
				if (lpb == nullptr) {
					lpb_maxsize = 0;
					return 0;
				}

				lpb_maxsize = dwSize;

			}

			if (GetRawInputData((HRAWINPUT)lParam, RID_INPUT, lpb, &dwSize,
					sizeof(RAWINPUTHEADER)) != dwSize) {
				OutputDebugString(TEXT("GetRawInputData does not return correct size !\n"));
			}

			RAWINPUT* raw = (RAWINPUT*)lpb;

			if (raw->header.dwType == RIM_TYPEKEYBOARD) {

				auto hResult = StringCchPrintf(szTempOutput, STRSAFE_MAX_CCH, TEXT(" Kbd: make=%04x Flags:%04x Reserved:%04x ExtraInformation:%08x, msg=%04x VK=%04x \n"),
					raw->data.keyboard.MakeCode,
					raw->data.keyboard.Flags,
					raw->data.keyboard.Reserved,
					raw->data.keyboard.ExtraInformation,
					raw->data.keyboard.Message,
					raw->data.keyboard.VKey);

				if (raw->data.keyboard.VKey != debug_key)
					break;

				if( (raw->data.keyboard.Flags == 0) && (!key_press_state_windows_events) ) {

					key_press_state_windows_events = true;
					key_pressed_time_windows_events = std::chrono::high_resolution_clock::now();
					auto passed_time = key_pressed_time_windows_events - key_pressed_time_async_query;

					key_press_delay = std::chrono::duration<float, std::micro>(passed_time).count();

				}
				else if ((raw->data.keyboard.Flags == 1) && (key_press_state_windows_events)) {

					key_press_state_windows_events = false;
					key_released_time_windows_events = std::chrono::high_resolution_clock::now();
					auto passed_time = key_released_time_windows_events - key_released_time_async_query;

					key_release_delay = std::chrono::duration<float, std::micro>(passed_time).count();

				}
			}

			else if (raw->header.dwType == RIM_TYPEMOUSE) {

				mouse_events_queue.push(0);

				auto hResult = StringCchPrintf(szTempOutput, STRSAFE_MAX_CCH, TEXT("Mouse: usFlags=%04x ulButtons=%04x usButtonFlags=%04x usButtonData=%04x ulRawButtons=%04x lLastX=%04x lLastY=%04x ulExtraInformation=%04x\r\n"),
					raw->data.mouse.usFlags,
					raw->data.mouse.ulButtons,
					raw->data.mouse.usButtonFlags,
					raw->data.mouse.usButtonData,
					raw->data.mouse.ulRawButtons,
					raw->data.mouse.lLastX,
					raw->data.mouse.lLastY,
					raw->data.mouse.ulExtraInformation);

				if (FAILED(hResult)) {
					assert(false);
				}

			}

			break;
		}

		default:

			return DefWindowProc(hWnd, message, wParam, lParam);

	}

	return 0;
}
