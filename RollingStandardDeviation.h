/*
Copyright (c) 2023 netborg

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. */

#pragma once

#include <boost/accumulators/statistics/rolling_variance.hpp>



class RollingStandardDeviation {
	
public:

	RollingStandardDeviation( int aSampleCount );
	~RollingStandardDeviation();
	
	void push( double aValue );
	void getResult( float& aMean, float& aStandardDeviation );
	
private:
	
	// default calculation is lazy (in the result extractor)
	typedef boost::accumulators::accumulator_set<
		double,
		boost::accumulators::stats< boost::accumulators::tag::rolling_variance > > acc_t;
		
	acc_t* mAcc;
	
};




inline RollingStandardDeviation::RollingStandardDeviation( int aSampleCount ) {
	mAcc = new acc_t(boost::accumulators::tag::rolling_window::window_size = aSampleCount );
}


inline RollingStandardDeviation::~RollingStandardDeviation() {
	delete mAcc;
}


inline void RollingStandardDeviation::push( double aValue ) {
	(*mAcc)(aValue);
}


inline void RollingStandardDeviation::getResult( float& aMean, float& aStandardDeviation ) {
	aMean = (float) boost::accumulators::rolling_mean(*mAcc);
	aStandardDeviation = (float) std::sqrt(boost::accumulators::rolling_variance(*mAcc));
}

