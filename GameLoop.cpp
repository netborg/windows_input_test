/*
Copyright (c) 2023 netborg

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. */

#include "GameLoop.h"
#include "RollingStandardDeviation.h"
#include <thread>
#include <assert.h>
#include <windows.h>


mouse_events_query_t mouse_events_query_type = PEEK_MESSAGE;
int debug_key = 0x44;

//boost::lockfree::queue<int64_t, boost::lockfree::capacity<2048> > mouse_events_queue;
boost::lockfree::spsc_queue<int64_t, boost::lockfree::capacity<2048> > mouse_events_queue;

std::atomic<float> mouse_events_stats_avg(0.0f);
std::atomic<float> mouse_events_stats_std_dev(0.0f);


bool key_press_state_windows_events;
bool key_press_state_async_query;

time_point_t key_pressed_time_windows_events;
time_point_t key_pressed_time_async_query;

time_point_t key_released_time_windows_events;
time_point_t key_released_time_async_query;


std::atomic<float> key_press_delay(0.0f);
std::atomic<float> key_release_delay(0.0f);


std::atomic_bool game_loop_is_running(false);
std::thread* game_loop_thread = nullptr;

RollingStandardDeviation rolling_std_dev(2500);




void busy_wait(int aMicroseconds) {

	auto start_time = std::chrono::high_resolution_clock::now();
	while (true) {
		auto duration = std::chrono::high_resolution_clock::now() - start_time;
		if (std::chrono::duration<float, std::micro>(duration).count() > aMicroseconds)
			break;
	}

}



void start_game_loop() {

	assert(game_loop_thread == nullptr);
	assert(game_loop_is_running == false);
	assert(mouse_events_query_type == GET_MESSAGE);

	game_loop_is_running = true;
	game_loop_thread = new std::thread(game_loop_main);

}


void end_game_loop() {

	assert(game_loop_thread != nullptr);
	assert(game_loop_is_running == true);
	assert(mouse_events_query_type == GET_MESSAGE);

	game_loop_is_running = false;
	game_loop_thread->join();
	delete game_loop_thread;
	game_loop_thread = nullptr;

}



void game_loop_main() {

	while (game_loop_is_running) {

		if (mouse_events_query_type == PEEK_MESSAGE ) {

			bool peek_result = true;
			MSG msg;
			while (peek_result) {

				auto start = std::chrono::high_resolution_clock::now();
				peek_result = PeekMessage(&msg, NULL, 0, 0, PM_REMOVE | PM_NOYIELD);
				auto end = std::chrono::high_resolution_clock::now();
				auto passed_time = end - start;

				if (peek_result) {
					TranslateMessage(&msg);
					DispatchMessage(&msg);
				}
			}
		}

		SHORT async_key_state = GetAsyncKeyState(debug_key);
		if ((1 << 15) & async_key_state) {
			if (!key_press_state_async_query) {
				key_press_state_async_query = true;
				key_pressed_time_async_query = std::chrono::high_resolution_clock::now();
				auto passed_time = key_pressed_time_windows_events - key_pressed_time_async_query;
				key_press_delay = std::chrono::duration<float, std::micro>(passed_time).count();
			}
		}
		else {
			if (key_press_state_async_query) {
				key_press_state_async_query = false;
				key_released_time_async_query = std::chrono::high_resolution_clock::now();
				auto passed_time = key_released_time_windows_events - key_released_time_async_query;
				key_release_delay = std::chrono::duration<float, std::micro>(passed_time).count();
			}
		}


		int64_t mouse_event;
		while (mouse_events_queue.pop(mouse_event)) {

			static auto time = std::chrono::high_resolution_clock::now();
			auto new_time = std::chrono::high_resolution_clock::now();
			auto passed_time = new_time - time;
			time = new_time;

			rolling_std_dev.push(
				std::chrono::duration<float, std::micro>(passed_time).count()
			);

			float avg, std_dev;
			rolling_std_dev.getResult(avg, std_dev);

			mouse_events_stats_avg = avg;
			mouse_events_stats_std_dev = std_dev;

		}

	}

}
