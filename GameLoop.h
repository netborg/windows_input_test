/*
Copyright (c) 2023 netborg

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE. */

#pragma once

#include <boost/lockfree/queue.hpp>
#include <boost/lockfree/spsc_queue.hpp>
#include <atomic>
#include <chrono>

void start_game_loop();
void end_game_loop();

void game_loop_main();
void busy_wait(int aMicroseconds);

typedef std::chrono::time_point<std::chrono::high_resolution_clock> time_point_t;
typedef enum {
	PEEK_MESSAGE = 0,
	GET_MESSAGE
} mouse_events_query_t;

extern mouse_events_query_t mouse_events_query_type;

extern std::atomic_bool game_loop_is_running;

//extern boost::lockfree::queue<int64_t, boost::lockfree::capacity<2048> > mouse_events_queue;
extern boost::lockfree::spsc_queue<int64_t, boost::lockfree::capacity<2048> > mouse_events_queue;
extern std::atomic<float> mouse_events_stats_avg;
extern std::atomic<float> mouse_events_stats_std_dev;

extern bool key_press_state_windows_events;
extern bool key_press_state_async_query;

extern int debug_key;

extern time_point_t key_pressed_time_windows_events;
extern time_point_t key_pressed_time_async_query;

extern time_point_t key_released_time_windows_events;
extern time_point_t key_released_time_async_query;

extern std::atomic<float> key_press_delay;
extern std::atomic<float> key_release_delay;

